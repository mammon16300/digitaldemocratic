#!/bin/bash
source ../digitaldemocratic.conf

echo "Dump realm.json"
docker exec -i isard-sso-keycloak sh -c '/opt/jboss/keycloak/bin/kcadm.sh \
    config credentials --server http://localhost:8080/auth \
    --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD &> /dev/null && \
    /opt/jboss/keycloak/bin/kcadm.sh \
    get realms/master' > keycloak/realm.json

echo "Dump clients.json"
docker exec -i isard-sso-keycloak sh -c '/opt/jboss/keycloak/bin/kcadm.sh \
    config credentials --server http://localhost:8080/auth \
    --realm master --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD &> /dev/null && \
    /opt/jboss/keycloak/bin/kcadm.sh \
    get clients' > keycloak/clients.json

kcadm.sh create realms -f - << EOF
{ "realm": "demorealm", "enabled": true }
EOF


### NEW

./kcadm.sh update realms/master -f realm.json