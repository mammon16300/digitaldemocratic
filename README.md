# What's this

This project allows to bring up with ease a full identity provider and many apps to have an environment thought for schools and universities. The project will provide an integrated solution to handle the common environment in education:

- **Classrooms**: A moodle instance with custom theme and custom plugins.
- **Cloud drive**: A nextcloud instance with custom theme and custom plugins.
- **Documents**: An onlyoffice instance integrated with nextcloud.
- **Web pages**: A wordpress instance with custom theme and custom plugins.
- **Pad**: An etherpad instance integrated with nextcloud.
- **Conferences**: A BigBlueButton integrated with moodle and nextcloud. This needs an standalone host right now 
- **Forms**: A forms nextcloud plugins.
- ... (some apps are there like jitsi or BigBlueButton but not fully integrated right now)

|                              |                                 |
| ---------------------------- | ------------------------------- |
| ![](docs/img/classrooms.png) | ![](docs/img/cloud_storage.png) |

# Administration interface

Now there is an administration interface that allows to easily manager users and groups and keep it in sync between applications. This is done by executing actions on the applications apis.

| ![](docs/img/admin_sync.png) | ![](docs/img/admin_user_edit.png) |
| ---------------------------- | --------------------------------- |

To easily migrate and populate system there are also two provided imports for users and groups:

- From Google Admin Console as a JSON dump
- From a CSV file

This admin interface is now in an alpha status but allows to manage users in sync between keycloak, moodle and nextcloud.

# Project status

It works but as we are working on it it will have lots of improvements upcoming months. Some automatizations need to be done, specially with SAML integration in moodle and Keycloak initial personalization.

Your collaboration is wellcome! Just fork this repo and do a PR or open us an issue.

# DigitalDemocratic Documentation

This documentation is writed in Markdown using [MkDocs+Gitlab](https://gitlab.com/pages/mkdocs).

See `docs` directory for Markdown files or the [auto-built site](https://digitaldemocratic.gitlab.io/digitaldemocratic).

## Quick start

```
cp digitaldemocratic.conf.sample digitaldemocratic.conf
```

Change default passwords

```
./securize_conf.sh
```

Edit digitaldemocratic.conf file variables to suit your needs.

```
cp -R custom.sample custom
```

Edit and replace files to personalize system.

```
make all
```

NOTE: The SAML Auth in plugin automation status now is:
- Moodle: Not fully automated.
    1. Login to moodle as admin via: https://moodle.\<domain\>/login/index.php?saml=off
    2. Go to authentication configuration: https://moodle.\<domain\>/admin/settings.php?section=manageauths
    3. Enable SAML2 clicking the eye.
    4. Clic on *configuration* on SAML2
    5. Click on the *Regenerate certificate* button inside the form. After that go back to SAML2 configuration page.
    6. Click on the *Lock certificate* button.
    7. In the terminal execute the script to autoconfigure: docker exec isard-sso-admin python3 moodle_saml.py
    8. The last thing is to purge moodle cache: [[missing docker exec php-fpm7 script, do it through moodle web ui]]

- Nextcloud: Fully automated. After finishing the *make all* should be ready. In case it fails refer to isard-sso/docs.
- Wordpress: Fully automated. After finishing the *make all* should be ready. In case it fails refer to isard-sso/docs.

## Extended

You can start this project in any docker & docker-compose host (any OS should work). To install those packages in your distro refer to docker & docker-compose in this documentation and in the sysadm folder you have some scripts.

Any distro should work but, if you want to use our sysadm scripts to install docker & docker compose use Debian Buster (10).

### Clone and submodules

```
git clone https://gitlab.com/digitaldemocratic/digitaldemocratic/
cd digitaldemocratic
git submodule update --init --recursive
```

### docker

Refer to the official documentation (https://docs.docker.com/engine/install/) or use our sysadm script if you are on a Debian Buster (10).

### docker-compose

Refer to the official documentation (https://docs.docker.com/compose/install/) or use our sysadm script if you are on a Debian Buster (10).

### Settings

Copy digitaldemocratic.conf.example to digitaldemocratic.conf and edit to suit your needs. At least (for development) you need to adapt the DOMAIN variable to your root domain.

- PRODUCTION: You need a multidomain dns (or redirect the multiple subdomains) to your host machine.
- DEVELOPMENT: You can just edit your /etc/hosts and add all of them there just for testing purposes.

#### Subdomains
- Keycloak: sso.<yourdomain.org>
- FreeIPA: ipa.<yourdomain.org>
- Api: api.<yourdomain.org>
- Moodle: moodle.<yourdomain.org>
- Nextcloud: nextcloud.<yourdomain.org>
- Wordpress: wp.<yourdomain.org>
- Onlyoffice: oof.<yourdomain.org>
- Etherpad: pad.<yourdomain.org>

### Personalization

Copy recursively the *custom.sample* folder to *custom* and edit the custom/menu yaml files and replace images.

### Bring the project up
```
make all
```

### Integration

Please read the [SAML_README.md](https://gitlab.com/isard/isard-sso/-/blob/master/docs/SAML_README.md) in isard-sso/docs folder to integrate all apps. Now nextcloud and wordpress should be automatically integrated with keycloak after finishing the make all but moodle needs some manual steps yet.
